# Closed Question Essay

Provides an "Essay" question type to the Closed Question module. Essay
questions solicit a configurable number of paragraphs from the user.

## Features

Closed Question Essay provides:

* A "Saved Writing" tab on user profiles that lists all essay
  submissions
* A simple page to view an essay submission
* A special ephemeral page for anonymous users to view their essay
  submissions

## Notes

* Essay submissions from anonymous users are not stored in the database.
  (This is the same for all Closed Question answers.) Thus the module
  presents messages urging the user to log in instead.
* There is currently no way to "convert" your essay submission as an
  anonymous user to associate it with your user account.

## Requirements

* Drupal 7
* [Closed Question](http://drupal.org/project/closedquestion) version
  7.x-2.2+

