<?php

/**
 * @file
 * Closed Question Question class.
 */

class CqQuestionEssay extends CqQuestionAbstract {
  /**
   * Array of feedback items.
   */
  protected $feedback = array();

  /**
   * The base-name used for form elements that need to be accessed by
   * JavasSript.
   *
   * @var string
   */
  protected $formElementName = '';

  /**
   * List of paragraphs for the student to write.
   *
   * @var array of paragraph arrays, where each paragraph array has this data:
   * - text: The message that goes above the textarea.
   * - rows: The number of rows for the textarea.
   */
  protected $paragraphs = array();

  /**
   * HTML containing the question-text.
   *
   * @var string
   */
  protected $text;

  /**
   * Constructs an Essay question object.
   *
   * @param CqUserAnswerInterface $userAnswer
   *   The CqUserAnswerInterface to use for storing the student's answer.
   * @param object $node
   *   Drupal node object that this question belongs to.
   */
  public function __construct(CqUserAnswerInterface &$userAnswer, &$node) {
    parent::__construct();
    $this->userAnswer = $userAnswer;
    $this->node = &$node;
    $this->formElementName = 'essay-question-' . $this->node->nid;
    $this->knownElements = array('paragraph', 'paragraphs');
  }

  /**
   * Implements CqQuestionAbstract::checkCorrect().
   */
  public function checkCorrect() {
    // No grading. Any answer is considered "correct" for this question type.
    return TRUE;
  }

  /**
   * Implements CqQuestionAbstract::getAllText().
   */
  public function getAllText() {
    $this->initialise();
    $build = array();
    $build['text'] = array(
      '#markup' => t('<label>Question text:</label><div>@text</div>', array('@text' => $this->text)),
    );
    $build['paragraphs'] = array(
      '#theme' => 'item_list',
      '#title' => t('Paragraph descriptions'),
      '#items' => array(),
      '#type' => 'ol',
    );
    foreach ($this->paragraphs as $item) {
      $build['paragraphs']['#items'][] = $item['text'];
    }
    return $build;
  }

  /**
   * Implements CqQuestionAbstract::getFeedbackItems().
   */
  public function getFeedbackItems() {
    return $this->feedback;
  }

  /**
   * Implements CqQuestionAbstract::getForm().
   */
  public function getForm($form_state) {
    global $user;
    if ($user->uid == 0) {
      drupal_set_message(t("Warning: Your writing cannot be saved unless you're logged in. We recommend you !login or !register now.",
        array(
          '!login' => l(t('log in'), 'user/login'),
          '!register' => l(t('register'), 'user/register'),
        )), 'warning');
    }
    $form = array();
    $index = 1;
    $answers = $this->userAnswer->getAnswer();

    // Wrap the entire question in a div.
    $form['essay'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('cq-essay-wrapper', 'cq-question-wrapper')),
    );

    // Put the question text first, if there is any.
    if (!empty($this->text)) {
      $form['essay']['questiontext'] = array(
        '#type' => 'item',
        '#markup' => $this->text,
        '#weight' => 0,
      );
    }

    $weight = 1;
    foreach ($this->paragraphs as $paragraph) {
      // Put in the paragraph's description text (the prompt for what to write
      // in the textarea).
      $form['essay']['description-' . $index] = array(
        '#type' => 'item',
        '#markup' => $paragraph['text'],
        '#weight' => $weight++,
      );

      $id = 'paragraph-' . $index;
      $form['essay'][$id] = array(
        '#type' => 'textarea',
        '#title' => t('Paragraph !index', array('!index' => $index)),
        '#title_display' => 'invisible',
        '#parents' => array('paragraphs', $id),
        '#rows' => $paragraph['rows'],
        '#default_value' => isset($answers[$id]) ? $answers[$id] : '',
        '#weight' => $weight++,
      );
      $index++;
    }

    $wrapper_id = 'cq-feedback-wrapper_' . $this->formElementName;
    $this->insertFeedback($form, $wrapper_id);
    $this->insertSubmit($form, $wrapper_id);
    $form['submit']['#value'] = t('Save Writing');

    return $form;
  }

  /**
   * Implements CqQuestionAbstract::getOutput().
   */
  public function getOutput() {
    $this->initialise();
    $retval = drupal_get_form('closedquestion_get_form_for', $this->node);
    $retval['#prefix'] = $this->prefix;
    $retval['#suffix'] = $this->postfix;
    return $retval;
  }

  /**
   * Overrides CqQuestionAbstract::loadXml().
   */
  public function loadXml(DOMElement $dom) {
    parent::loadXml($dom);
    module_load_include('inc.php', 'closedquestion', 'lib/XmlLib');

    foreach ($dom->childNodes as $node) {
      $name = drupal_strtolower($node->nodeName);
      switch ($name) {
        case 'text':
          $this->text = cq_get_text_content($node, $this);
          break;

        case 'paragraphs':
          // We don't need to do anything for this wrapper tag.
          break;

        case 'paragraph':
          /* @var $rows DOMAttr */
          $rows = $node->attributes->getNamedItem('rows');
          $this->paragraphs[] = array(
            'text' => cq_get_text_content($node, $this),
            'rows' => $rows ? $rows->value : 6,
          );
          break;

        default:
          break;
      }
    }
  }

  /**
   * Implements CqQuestionAbstract::submitAnswer().
   */
  public function submitAnswer($form, &$form_state) {
    $this->userAnswer->setAnswer($form_state['values']['paragraphs']);

    // Save their answer if it's changed.
    if ($this->userAnswer->answerHasChanged()) {
      // For essays, "tries" keeps track of how many times the user has saved.
      $this->userAnswer->increaseTries();
      $this->userAnswer->store();
    }

    // Clear out any existing feedbacks.
    $this->feedback = array();

    global $user;
    if ($user->uid == 0) {
      $fb = new CqFeedback();
      $message = t("You can temporarily view your writing !here, where you can save it to your computer or print it.",
        array('!here' => l(t('on this page'), 'view-essay/' . $this->node->nid)));
      $fb->initWithValues($message, 0, 9999);
      $this->feedback[] = $fb;
    }
    else {
      $here = l(t('here'), 'user/' . $user->uid . '/saved-writing/' . $this->node->nid);
      $saved_writing = l(t('Saved Writing'), 'user/' . $user->uid . '/saved-writing');
      $message = t("Great! We've saved your essay. It can be viewed !here. Find it later under the !saved-writing tab of your profile.",
        array('!here' => $here, '!saved-writing' => $saved_writing));

      $fb = new CqFeedback();
      $fb->initWithValues($message, 0, 9999);
      $this->feedback[] = $fb;

      $fb = new CqFeedback();
      $fb->initWithValues(t("Feel free to keep working on your essay on this page, now or later."), 0, 9999);
      $this->feedback[] = $fb;
    }
  }
}
