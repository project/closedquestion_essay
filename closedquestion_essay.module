<?php

/**
 * @file
 * Closed Question Essay module. Provides an Essay question type that solicits
 * a configurable number of paragraphs from the user.
 */

/**
 * Implements hook_menu().
 */
function closedquestion_essay_menu() {
  $items = array();
  $items['user/%user/saved-writing'] = array(
    'title' => 'Saved Writing',
    'access callback' => 'closedquestion_essay_listing_access',
    'access arguments' => array(1),
    'page callback' => 'closedquestion_essay_listing_page',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  $items['user/%user/saved-writing/%node'] = array(
    'title callback' => 'closedquestion_essay_view_title',
    'title arguments' => array(1, 3),
    'access callback' => 'closedquestion_essay_view_access',
    'access arguments' => array(1, 3),
    'page callback' => 'closedquestion_essay_view_page',
    'page arguments' => array(1, 3),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  // Create a temporary essay-viewing page for anonymous users.
  $items['view-essay/%node'] = array(
    'title' => 'Your Writing',
    'access callback' => 'closedquestion_essay_view_access',
    'access arguments' => array(NULL, 1),
    'page callback' => 'closedquestion_essay_view_page',
    'page arguments' => array(NULL, 1),
  );
  return $items;
}

/**
 * Menu item title callback for essay view page.
 */
function closedquestion_essay_view_title($account, $node) {
  return t('Saved Writing: @title', array('@title' => $node->title));
}

/**
 * Menu item access callback.
 */
function closedquestion_essay_listing_access($account) {
  global $user;
  return ($account->uid == $user->uid) || user_access('administer users');
}

/**
 * Menu item access callback.
 */
function closedquestion_essay_view_access($account, $node) {
  global $user;
  if (!$account) {
    $account = $user;
  }

  return
    ($node->type == 'closedquestion') &&
    ($node->question instanceof CqQuestionEssay) &&
    (($account->uid == $user->uid) || user_access('administer users'));
}

/**
 * Page callback for Saved Writing listing page.
 */
function closedquestion_essay_listing_page($account) {
  global $user;
  $title = $account->uid == $user->uid ? t('My Saved Writing') : t('Saved Writing');
  drupal_set_title($title);
  return views_embed_view('closedquestion_essay_writing', 'default', $account->uid);
}

/**
 * Page callback: Renders a simple page with the user's essay for the
 * given node.
 */
function closedquestion_essay_view_page($account, $node) {
  global $user;
  if (!$account) {
    $account = $user;
  }
  drupal_set_title(t('Saved Writing: @question', array('@question' => $node->title)));

  // Retrieve the answer (paragraphs array) for the given question/user pair.
  $answer = new CqUserAnswerDefault($node->nid, $account->uid);
  $answer = $answer->getAnswer();

  $essay = '';
  foreach ($answer as $paragraph) {
    $essay .= '<p>' . $paragraph . '</p>';
  }
  $essay .= '<a class="cq-essay-print button" href="javascript:print()">' . t('Print') . '</a>';
  return $essay;
}

/**
 * Implements hook_closedquestion_question_types().
 */
function closedquestion_essay_closedquestion_question_types() {
  return array('essay');
}

/**
 * Implements hook_closedquestion_question_factory().
 */
function closedquestion_essay_closedquestion_question_factory($type, &$user_answer, &$node) {
  return ($type == 'essay') ? new CqQuestionEssay($user_answer, $node) : NULL;
}

/**
 * Implements hook_closedquestion_xml_config_alter().
 */
function closedquestion_essay_closedquestion_xml_config_alter(&$xml_config) {
  $xml_config['xml_specific_config'][] = array(
    'conditions' => array(
      'family_node' => 'question',
      'attribute' => 'type',
      'attributeValue' => 'essay',
    ),
    'config_changes' => array(
      'types' => array(
        'question' => array(
          'valid_children' => array(
            'text',
            'paragraphs',
          ),
        ),
      ),
    ),
  );
  $xml_config['types']['paragraphs'] = array(
    'content' => 0,
    'description' => 'Collection of essay paragraphs.',
    'attributes' => array(),
    'valid_children' => array('paragraph'),
    'max_children' => 0,
    'max_count' => 1,
    'is_group' => 1,
  );
  $xml_config['types']['paragraph'] = array(
    'content' => 1,
    'description' => 'Essay paragraph text entry box',
    'attributes' => array(),
    'valid_children' => array(),
    'max_children' => 0,
    'max_count' => -1,
    'icon' => array('image' => 'icons/text_icon.png'),
    'in_group' => 'paragraphs',
  );
  $xml_config['types']['question']['valid_children'][] = 'paragraphs';
  $xml_config['types']['question']['valid_children'][] = 'paragraph';
}

/**
 * Implements hook_closedquestion_templates().
 */
function closedquestion_essay_closedquestion_templates() {
  return array(
    'essay' => array(
      'name' => 'Essay',
      'xml' => '<question type="essay">
  <text>Question introduction message goes here.</text>
  <paragraph>First, write your introduction.</paragraph>
  <paragraph>Now write a paragraph about your first topic.</paragraph>
  <paragraph>Now write a paragraph about your second topic.</paragraph>
  <paragraph>Now write a paragraph about your third topic.</paragraph>
  <paragraph>Now write your conclusion.</paragraph>
</question>
      ',
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function closedquestion_essay_views_api() {
  return array('api' => 3);
}
